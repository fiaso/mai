Лабароторная работа #4 (web)

## Лабароторная работа #4 (web)
**Установим nginx, php, php-fpm**
```bash
apt-get update
apt-get install nginx -y
apt-get install php -y
apt-get install php-fpm -y
```

**Установить mysql**
- при установке будет запрошен пароль, задайте и запомните
- в разных версиях процедура с паролем может отличаться, смотрите документацию
```bash
apt-get install wget
wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb
sudo dpkg -i percona-release_latest.$(lsb_release -sc)_all.deb
percona-release setup ps57
sudo apt-get install percona-server-server-5.7
apt-get install php-mysql -y
```

**Создать базу для wordpress**

```bash
mysql -u root -p<Пароль который задали при установке>
mysql> CREATE DATABASE db_sofia DEFAULT CHARACTER SET utf8mb4;
mysql> GRANT ALL ON *.* TO 'sofia'@'%' IDENTIFIED BY 'sofia';
mysql> FLUSH PRIVILEGES;
```

![43e04df18419e88c444ffbf1c00d5acb.png](../_resources/9e34c890344c4d0bb585d00b16205191.png)


**Установить wordpress**

```bash
cd /tmp
curl -O https://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz -C /var/www/
cd /var/www/wordpress/
cp wp-config-sample.php wp-config.php
chown -R www-data:www-data /var/www/wordpress/
find /var/www/wordpress/ -type d -exec chmod 750 {} \;
find /var/www/wordpress/ -type f -exec chmod 640 {} \;
```

**Отредактируем конфигурационный файл wp-config.php**
- получите список ключей
```bash
curl -s https://api.wordpress.org/secret-key/1.1/salt/
```
-  пропишите их в конфиге вместо
```bash
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );
```
![855bed0fd76d90d1390d1ea271a1d47e.png](../_resources/7aa189a0c47345c29f9cd2a7ee88dbcf.png)


- пропишите доступы к БД
```bash
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpressuser');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

```
![056aaa7e923d6926395811a72a1e5388.png](../_resources/3924a19c807d4423b7052df3a0884056.png)


**Настроим php-fpm**

- отредактируем /etc/php/7.2/fpm/pool.d/www.conf
    - `chdir = /var/www/wordpress`

![207e32e6a6bbf665de6a79c9fc172560.png](../_resources/a6c4c90feacb4cd88470a36774fc61a6.png)

- уточним имя сокета
    - `listen = /run/php/php7.2-fpm.sock`

![eb174d8488657dae222a12b44dfd2b9e.png](../_resources/6b42502ace2a4dfdba7b050dd013d08b.png)

- зарелоадим сервис
    - `systemctl reload php7.2-fpm.service`

![bcb4d4d54a62bf2e4bfb44b1c9a0a2d9.png](../_resources/db2579926c384a838b26bd1101ef10c9.png)

    
**Настроим nginx**
- создайте файл /etc/nginx/sites-available/wordpress
```bash
server {
  listen 8080;

  access_log /var/log/nginx/wordpress_access.log;
  error_log /var/log/nginx/wordpress_error.log;
    root   /var/www/wordpress;
    index index.php index.html index.htm;

    location / {
        try_files $uri $uri/ =404;
    }
    error_page 404 /404.html;
    location = /50x.html {
        root /var/www/wordpress;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

}
```
![36265e68796800ea064fe74a81fc1510.png](../_resources/35414ac34265488f8cb71c78b42a4075.png)

- создайте линк в /etc/nginx/sites-enabled
- сделайте релоад сервиса
- добавьте в README результат `ls -l /etc/nginx/sites-enabled`

![f3bf5635381385ab32f2420e8054fd2a.png](../_resources/e10236cbd0334b0ca318a871970fe818.png)

**Включим расширение mysqli в /etc/php/7.2/fpm/php.ini**
```bash
extension=mysqli
```
- релоад сервиса  php-fpm
- добавьте в README список все активных опций (без закомментированных) из /etc/php/7.2/fpm/php.ini

![b7828090ba30e7563738b91056aba235.png](../_resources/d2ad74f172f74b76b00487e2802c943b.png)

![fd52d353b3bb5f2d6fbefa413eae13e5.png](../_resources/5fa7ab91fa704dbeb5150627922a2cd9.png)


**Зайдем из броузера**

- откройте в броузере http://<your_ip>:8080
- следуйте указаниям установите вордпресс
- создайте пост с вашей ФИО и номером группы
- скриншот добавьте в README

![0b674c463e616a97a42918c5d8c8600b.png](../_resources/6f485c99033b4299ab1547e0f295e954.png)

К сожалению, кроме данной страницы ничего получить так и не удалось.

![805bf51141a640c4b4dece203a8093ae.png](../_resources/e32735511fef418ba321830f199570b9.png)

**Разделение по имени**
- в файле /etc/hosts пропишите имя сервера <server_name> на ваш выбор
- в конфиге нжинкса исправьте 
`  listen 8080;`
на 
```  
listen 8080;
server_name  <server_name>;
```
- сделайте релоад нжинкса
- зайдите из броузера по заданному имени сервиса
- скриншот добавить в README


**Установим yandex tank**
```bash
apt-get install python-pip build-essential python-dev libffi-dev gfortran libssl-dev
sudo -H pip install --upgrade setuptools
sudo -H pip install https://api.github.com/repos/yandex/yandex-tank/tarball/master
sudo add-apt-repository ppa:yandex-load/main
apt-get update
apt-get install phantom phantom-ssl
```

**Создадим конфиг для yandex tank**
```bash
mkdir /tmp/tank
cd /tmp/tank
```


![4c256867751a7e9f24f0f453faa0b017.png](../_resources/29b10a95adfc457ead531b222f3edd7d.png)


- создадим файл конфига load.yaml
```yaml
phantom:
  address: <your_ip>:8080 # [Target's address]:[target's port]
  instances: 300
  uris:
    - /
  load_profile:
    load_type: rps # schedule load by defining requests per second
    schedule: step(20, 350, 15, 5) # starting from 1rps growing linearly to 10rps during 10 minutes
#    schedule: const(200, 300)
#    schedule: const(200, 20)
console:
  enabled: true # enable console output
telegraf:
  enabled: false # let's disable telegraf monitoring for the first time
```


![57fba62bbd1f8b870bef75bebe172640.png](../_resources/e10655cf56d5445fb6a3dade2a319073.png)


- запустим тестирование
`yandex-tank -c load.yml`
- скриншот приложить в README

**Понаблюдать за статистикой**
- в отдельном терминали проанализировать нагрузку
- написать свои выводы
- увеличить кол-во воркеров и сравнить тесты
