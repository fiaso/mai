Лабароторная работа #1 (ядро+systemd)*

## Лабароторная работа #1 (ядро+systemd)
*1) Вход в машину в single mode и смена пароля root*

![1a39df04b1066324f97c9015c3449118.png](../_resources/81d99d6ea52a43a9b416c7e24324a95c.png)

![71c3e072b3035fcfb5e9fc0ad27f0369.png](../_resources/f761c3255db34d5e96550158fcc37205.png)

![ca4ebca931924856a57f378b0421e181.png](../_resources/121ba068f34541a58e1766802e7dc830.png)

*2) Bash-скрипт, который выясняет 5 самых "тяжелых" процессов и пишет их в лог каждые 10 секунд*

![0635e976db95b57b60b429af370a3248.png](../_resources/f2fa6b5a09c74df5bc96422df141c93b.png)

![a5d9664ba58d77c24d1fef0d405b5d0f.png](../_resources/cc5b3e86afef4bb6b3f130580833104d.png)

*3) Запуск и мониторинг скрипта systemd юнитом, в случае падения - перезапуск скрипта*

![dec54a112c5fccc1ec799fa7b5b66c49.png](../_resources/78a25dc2673644269ed29352e5ac3401.png)

![893c3ca707d66d126061b72e59842fbd.png](../_resources/24b9a297ccce4f65954ba16fe342bd14.png)

![e7dc3bfdddd7c230d067e7db72e3de44.png](../_resources/8bb997f636b24960b026265f4079a56c.png)

